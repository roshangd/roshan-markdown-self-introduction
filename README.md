# Roshan Markdown Self Introduction

Hello, here is a markdown version of myself.

# Roshan George Daniel

## Hi, that's me
<img src="/uploads/d39d4a67c16ec5d303d42aa391e3f357/Roshan_George_Daniel_HD_photo.jpeg" width="200" height="200" />

## About myself
Final year aerospace undergrad, putting an emphasis on drone technology :helicopter: . I'm eager to upskill within the field of AI and ML where it is crucial to the further development of drone technology.

## I am from .. 
Cameron Highlands :sunrise_over_mountains:

Lets get dem strawberries :strawberry: 

## My Strengths and Weaknesses
Strength | Weakness
------------ | -------------
Driven | 'Yes' Person
Eager to Learn | Too much of a generalist

## My next move
1. Making use of the opportunity to learn new skills in IDP
1. Networking with new co-workers
1. Practise good project management to get the ball running

## Here is a Quote for you!

> Ego is probably one of the biggest poisons we can have - it is toxic to any environment

*Jonny Kim*



